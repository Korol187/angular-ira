import 'bootstrap/less/bootstrap.less';
import './assets/css/ionicons.min.css';
import './assets/css/style.less';
import ng from 'angular';
import satellizer from 'satellizer';
import uiRouter from 'angular-ui-router';
import ngMessages from 'angular-messages';
import { AppConfig } from './index.config';
import Components from './app/components';
import Directives from './app/directives';
import Services from './app/services';

ng.module('app', [uiRouter, ngMessages, Components, Directives, satellizer, Services])
  .config(AppConfig)
  .run(['$rootScope', '$window', '$auth',
    function($rootScope, $window, $auth) {

      if ($auth.isAuthenticated()) {
        $rootScope.currentUser = JSON.parse($window.localStorage.currentUser);
      }
  }]);

export default 'app';
