export const AppConfig = ($urlRouterProvider, $stateProvider, $authProvider) => {
  'ngInject';
  $stateProvider
    .state('home', {
      url: '/',
      template: '<home></home>'
    })
    .state('login', {
      url: '/login',
      template: '<login></login>'
    })
    .state('signup', {
      url: '/signup',
      template: '<signup></signup>'
    })
    .state('detail', {
      url: '/detail',
      template: '<detail></detail>'
    });

    $urlRouterProvider.otherwise('/');

    $authProvider.loginUrl = API_URL + '/auth/login';
    $authProvider.signupUrl = API_URL + '/auth/signup';
    $authProvider.oauth2({
      name: 'instagram',
      url: API_URL + '/auth/instagram',
      redirectUri: 'http://localhost:8080/',
      clientId: 'd185c2b12d2a4487ad3dedf5ba83e3d4',
      requiredUrlParams: ['scope'],
      scope: ['likes'],
      scopeDelimiter: '+',
       authorizationEndpoint: 'https://api.instagram.com/oauth/authorize'
    })

};
