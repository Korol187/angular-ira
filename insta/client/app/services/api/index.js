import ng from 'angular';
import ApiService from './api.service';

export default ng.module('app.apiService', [])
  .service('apiService', ApiService)
  .name;
