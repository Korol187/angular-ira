export default class ApiService {
  static $inject = ['$http'];
  constructor($http) {
    this.$http = $http;
  }

  getFeed() {
    return this.$http.get(API_URL + '/api/feed');
  }

  getMediaById(id) {
    return this.$http.get(API_URL + '/api/media/' + id);
  }

  likeMedia(id) {
    return this.$http.put(API_URL + '/api/like', { mediaId: id });
  }
}
