import ng from 'angular';
import ApiService from './api';

export default ng.module('app.services', [ApiService])
  .name;
