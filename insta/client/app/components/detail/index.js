import ng from 'angular';

import DetailComponent from './detail.component';

export default ng.module('app.detail', [])
  .component('detail', DetailComponent)
  .name;
