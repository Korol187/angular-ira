import template from './detail.html';
import DetailController from './detail.controller'

export default {
  template,
  controller: DetailController,
}
