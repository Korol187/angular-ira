export default class DetailController {
  static $inject = ['$location', '$rootScope', 'apiService'];
  constructor($location, $rootScope, apiService) {
    this.$location = $location;
    this.$rootScope = $rootScope;
    this.apiService = apiService;
  }

  $(onInit) {
    let self = this;
    const mediaId = self.$location.path().split('/').pop();
    self.apiService.getMediaById(mediaId)
      .then((media) => {
        self.hasLiked = media.user_has_liked;
        self.photo = media;
      })
  }

  like() {
    let self = this;
    self.hasLiked = true;
    self.apiService.getMediaById(mediaId).error((data) => {
      sweetAlert('Error', data.message, 'error');
    });
  }
}
