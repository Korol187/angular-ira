export default class NavbarController {
  static $inject = ['$window', '$location', '$rootScope', '$auth'];
  constructor($window, $location, $rootScope, $auth) {
    this.$window = $window;
    this.$location = $location;
    this.$rootScope = $rootScope;
    this.$auth = $auth;
  }

}
