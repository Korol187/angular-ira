import template from './navbar.html';
import NavbarController from './navbar.controller'

export default {
  template,
  controller: NavbarController,
}
