import ng from 'angular';

import NavbarComponent from './navbar.component';

export default ng.module('app.navbar', [])
  .component('navbar', NavbarComponent)
  .name;
