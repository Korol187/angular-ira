import ng from 'angular';

import ngHome from './home';
import ngLogin from './login';
import ngSignup from './signup';
import ngDetail from './detail';
import ngNavbar from './navbar'


export default ng.module('app.components', [ngHome, ngLogin, ngSignup, ngDetail, ngNavbar])
  .name;
