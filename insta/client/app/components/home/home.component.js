import template from './home.html';
import HomeController from './home.controller'

export default {
  template,
  controller: HomeController,
}
