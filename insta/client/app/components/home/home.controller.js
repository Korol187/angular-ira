export default class HomeController {
  static $inject = ['$window', '$rootScope', '$auth', 'apiService'];
  constructor($window, $rootScope, $auth, apiService) {
    this.$window = $window;
    this.$rootScope = $rootScope;
    this.$auth = $auth;
    this.apiService = apiService;
  }

  $onInit() {
    let self = this;
    if (self.$auth.isAuthenticated() && (self.$rootScope.currentUser && self.$rootScope.currentUser.username)) {
      self.apiService.getFeed()
        .then((data) => {
          self.photos = JSON.parse(data);
        })
        .catch((err) => {
          console.log(JSON.parse(err));
        })
    }
  }


  // check if logged in
  isAuthenticated () {
    return this.$auth.isAuthenticated();
  }


  // connect email account with instagram
  linkInstagram () {
    let self = this;
    self.$auth.link('instagram')
      .then((res) => {
        self.$window.localStorage.currentUser = JSON.stringify(res.data.user);
        self.$rootScope.currentUser = JSON.parse(this.$window.localStorage.currentUser);
        self.apiService.getFeed()
          .then((data) => {
            self.photos = JSON.parse(data);
        });
      })
  }
}
