import ng from 'angular';

import HomeComponent from './home.component';

export default ng.module('app.home', [])
  .component('home', HomeComponent)
  .name;
