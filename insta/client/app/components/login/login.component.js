import template from './login.html';
import LoginController from './login.controller'

export default {
  template,
  controller: LoginController,
}
