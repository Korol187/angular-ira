export default class LoginController {
  static $inject = ['$window', '$location', '$rootScope', '$auth'];
  constructor($window, $location, $rootScope, $auth) {
    this.$window = $window;
    this.$location = $location;
    this.$rootScope = $rootScope;
    this.$auth = $auth;
  }

  instagramLogin() {
    this.$auth.authenticate('instagram')
      .then((res) => {
        this.$window.localStorage.currentUser = JSON.stringify(res.data.user);
        this.$rootScope.currentUser = JSON.parse(this.$window.localStorage.currentUser);
      })
      .catch((res) => {
        console.log(res.data);
      });
  }

  emailLogin() {
    let self = this;
    this.$auth.login({
        email: self.email,
        password: self.password
      })
      .then((res) => {
        this.$window.localStorage.currentUser = JSON.stringify(res.data.user);
        this.$rootScope.currentUser = JSON.parse(this.$window.localStorage.currentUser);
      })
      .catch((res) => {
        self.errorMessage = {};
        angular.forEach(res.data.message, (message, field) => {
          self.loginForm[field].$setValidity('server', false);
          self.errorMessage[field] = res.data.message[field];
        });
      })
  }
}
