import ng from 'angular';

import LoginComponent from './login.component';

export default ng.module('app.login', [])
  .component('login', LoginComponent)
  .name;
