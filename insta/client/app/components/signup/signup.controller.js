export default class SignupController {
  static $inject = ['$auth', '$location'];
  constructor($auth, $location) {
    this.$auth = $auth;
    this.$location = $location;
  }

  signup() {
    let self = this;
    let user = {
      email: self.email,
      password: self.password
    };
    // Satellizer
    self.$auth.signup(user)
      .then((res) => {
        self.$auth.setToken(res);
        self.$location.path('/');
      })
      .catch((res) => {
        console.log(res)
      });
  }
}
