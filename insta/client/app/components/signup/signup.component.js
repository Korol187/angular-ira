import template from './signup.html';
import SignupController from './signup.controller'

export default {
  template,
  controller: SignupController,
}
