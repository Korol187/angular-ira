import ng from 'angular';

import SignupComponent from './signup.component';

export default ng.module('app.signup', [])
  .component('signup', SignupComponent)
  .name;
