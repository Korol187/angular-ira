export default class ServerError {
    /*@ngInject*/
    constructor() {
        this.restrict = 'A';
        this.require = 'ngModel';
    }

    link(scope, element, attrs, ctrl) {
      element.on('keydown', function() {
        ctrl.$setValidity('server', true)
      });
    }
}
