import ng from 'angular';
import ServerError from './serverError.directive';

export default ng.module('app.serverError', [])
  .directive('serverError', () => new ServerError())
  .name;
