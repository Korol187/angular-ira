import ng from 'angular';
import ServerError from './serverError';

export default ng.module('app.directives', [ServerError])
  .name;
