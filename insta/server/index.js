const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const config = require('./config');
require('./libs/mongoose');
const app = express();

app.set('port', process.env.PORT || config.port);
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

const users = require('./routes/users');
app.use('/', users);

app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + app.get('port'));
});
