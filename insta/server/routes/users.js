const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const moment = require('moment');
const User = mongoose.model('User');
const bcrypt = require('bcryptjs');
const jwt = require('jwt-simple');
const async = require('asyncawait/async');
const await = require('asyncawait/await');
const request = require('request');
const config = require('../config');

const createToken = (user) => {
  const payload = {
    exp: moment().add(14, 'days').unix(),
    iat: moment().unix(),
    sub: user._id
  };
  return jwt.encode(payload, config.tokenSecret);
};

const isAuthenticated = (req, res, next) => {
  if (!req.header('Authorization')) {
    return res.status(400).send({
      message: 'You did not provide a JWT in the Authorization header.'
    });
  }
  const token = req.header('Authorization').split(' ')[1];
  const payload = jwt.decode(token, config.tokenSecret);
  const now = moment().unix();

  if (now > payload.exp) {
    return res.status(401).send({
      message: 'Token has expired.'
    });
  }
  console.log(payload.sub);

  User.findById(payload.sub, function(err, user) {
    if (!user) {
      return res.status(400).send({ message: 'User no longer exists.' });
    }

    req.user = user;
    next();
  })
};


router
  .post('/auth/login', async((req, res) => {
    const user = await (User.findOne({ email: req.body.email }, '+password'));
    if (!user) {
      return res.status(401).send({
        message: {
          password: 'Incorrect email or password'
        }
      });
    }
    const isMatch = await (bcrypt.compare(req.body.password, user.password));
    if (!isMatch) {
      return res.status(401).send({
        message: {
          password: 'Incorrect email or password'
        }
      });
    }
    user = user.toObject();
    delete user.password;
    const token = createToken(user);
    res.send({
      token: token,
      user: user
    });
  }))
  .post('/auth/signup', async((req, res) => {
    const existingUser = await (User.findOne({
      email: req.body.email
    }));
    if (existingUser) {
      return res.status(409).send({
        message: {
          email: 'Email is already taken.'
        }
      });
    }
    const user = new User({
      email: req.body.email,
      password: req.body.password
    });
    user.save(function() {
      let token = createToken(user);
      res.send({
        token: token,
        user: user
      });
    });
  }))
  .post('/auth/instagram', async((req, res) => {
    const accessTokenUrl = config.tokenUrl;
    const params = {
      client_id: req.body.clientId,
      redirect_uri: req.body.redirectUri,
      client_secret: config.clientSecret,
      code: req.body.code,
      grant_type: 'authorization_code'
    };

    // Exchange authorization code for access token.
    request.post({
      url: accessTokenUrl,
      form: params,
      json: true
    }, async((e, r, body) => {
      // Link user accounts

      if (req.header('Authorization')) {

        let existingUser = await (User.findOne({
          instagramId: body.user.id
        }));
        let token = req.header('Authorization').split(' ')[1];
        let payload = jwt.decode(token, config.tokenSecret);

        let localUser = await (User.findById(payload.sub, '+password'));
        if (!localUser) {
          return res.status(400).send({
            message: 'User not found.'
          });
        }
        // Merge two accounts
        if (existingUser) {
          existingUser.email = localUser.email;
          existingUser.password = localUser.password;
          localUser.remove();
          existingUser.save(() => {
            let token = createToken(existingUser);
            return res.send({
              token: token,
              user: existingUser
            });
          });
        } else {
          // Link current email account with the Instagram profile information.

          localUser.instagramId = body.user.id;
          localUser.username = body.user.username;
          localUser.fullName = body.user.full_name;
          localUser.picture = body.user.profile_picture;
          localUser.accessToken = body.access_token;
          localUser.save(() => {
            let token = createToken(localUser);
            res.send({
              token: token,
              user: localUser
            });
          });
        }
      } else {
        // Create a new user account or return an existing one.
        let existingUser = await (User.findOne({
          instagramId: body.user.id
        }));
        if (existingUser) {
          let token = createToken(existingUser);
          return res.send({
            token: token,
            user: existingUser
          });
        }
        const user = new User({
          instagramId: body.user.id,
          username: body.user.username,
          fullName: body.user.full_name,
          picture: body.user.profile_picture,
          accessToken: body.access_token
        });
        user.save(() => {
          let token = createToken(user);
          res.send({
            token: token,
            user: user
          });
        });
      }
    }));
  }))
  .get('/api/feed', isAuthenticated, async((req, res) => {
    const feedUrl = config.feedUrl;
    const params = {
      access_token: req.user.accessToken
    };

    console.log(params)

    request.get({
      url: feedUrl,
      qs: params,
      json: true
    }, function(error, response, body) {
      if (!error && response.statusCode == 200) {
        return res.send(body.data);
      }
      res.status(500).send(error);
    });
  }))
  .get('/api/media/:id', isAuthenticated, async((req, res) => {
    const mediaUrl = config.mediaUrl + req.params.id;
    const params = {
      access_token: req.user.accessToken
    };

    request.get({
      url: mediaUrl,
      qs: params,
      json: true
    }, function(error, response, body) {
      if (!error && response.statusCode == 200) {
        res.send(body.data);
      }
    });
  }))
  .post('/api/like', isAuthenticated, async((req, res) => {
    const mediaId = req.body.mediaId;
    const accessToken = {
      access_token: req.user.accessToken
    };
    const likeUrl = config.mediaUrl + mediaId + '/likes';

    request.post({
      url: likeUrl,
      form: accessToken,
      json: true
    }, function(error, response, body) {
      if (response.statusCode !== 200) {
        return res.status(response.statusCode).send({
          code: response.statusCode,
          message: body.meta.error_message
        });
      }
      res.status(200).end();
    });
  }));

    module.exports = router;
