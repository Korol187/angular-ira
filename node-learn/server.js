var db = require('db');
db.connect();
var User = require('./user');

function run() {
  var nick = new User('Nick');
  var max = new User('Max');
  nick.hello(max);

  console.log(db.getPhrase('Run success'));
}

if (module.parent) {
  exports.run = run;
} else {
  run();
}
