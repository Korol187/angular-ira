var phrase;

exports.connect = function() {
  phrase = require('./ru');
}

exports.getPhrase = function(name) {
  if (!phrase[name]) {
    throw new Error('no phrase ' + name);
  }
  return phrase[name];
}
