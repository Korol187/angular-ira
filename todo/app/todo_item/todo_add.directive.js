(function() {
  'use strict';

  function todoAdd() {
    return {
      restrict: 'E',
      templateUrl: 'app/todo_item/todo_add.html',
      replace: true,
      require: {
        TodoListController: '^^todoList',
      },
    }
  }

  angular
    .module('todoApp')
    .directive('todoAdd', todoAdd);

})();
