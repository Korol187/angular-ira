(function() {
  'use strict';

  function TodoController() {
    var vm = this;
    vm.isEditting = false;
  }

  angular
    .module('todoApp')
    .controller('TodoController', TodoController);
})();
