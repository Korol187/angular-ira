(function() {
  'use strict';

  function todo() {
    return {
      restrict: 'E',
      controller: 'TodoController',
      controllerAs: 'vm',
      templateUrl: 'app/todo_item/todo.html',
      replace: true,
      bindToController: true,
      scope: {
        todo: '='
      },
    }
  }

  angular
    .module('todoApp')
    .directive('todo', todo);

})();
