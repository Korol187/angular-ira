(function() {
  'use strict';

  function boardAdd() {
    return {
      restrict: 'E',
      templateUrl: 'app/board/board_add.html',
      replace: true,
      require: {
        BoardListController: '^^boardList',
      },
    }
  }

  angular
    .module('todoApp')
    .directive('boardAdd', boardAdd);

})();
