(function() {
  'use strict';

  function BoardService() {
    var BoardService = {};

    BoardService.delete = function(boards, index) {
      boards.splice(1, index);
    }

    return BoardService;
  }

  BoardService.$inject = [];

  angular
    .module('todoApp')
    .service('BoardService', BoardService);
})();
