(function() {
  'use strict';

  function board() {
    return {
      restrict: 'E',
      controller: 'BoardController',
      controllerAs: 'vm',
      templateUrl: 'app/board/board.html',
      replace: true,
      bindToController: true,
      scope: {
        board: '=',
      },
      require: {
        BoardListController: '^^boardList',
      },
    }
  }

  angular
    .module('todoApp')
    .directive('board', board);

})();
