(function() {
  'use strict';

  TodoListController.$inject = ['TodoListService'];

  function TodoListController(TodoListService) {
    var vm = this;
    vm.TodoListService = TodoListService;
    vm.newTitle = '';
  }

  //create new todo
  TodoListController.prototype.add = function() {
    this.TodoListService.add(this.todos, this.newTitle);
    this.newTitle = '';
  }

  //count of unfinished todo
  TodoListController.prototype.unfinishedTodo = function () {
    var i = 0;
    angular.forEach(this.todos, function(item) {
      if (item.isComplite == false) {
        i++;
      }
    })
    if (i > 0) {
      return 'Unfinished tasks: ' + i
    } else {
      return 'Good job! You don\'t have unfinished tasks'
    }

  };

  angular
    .module('todoApp')
    .controller('TodoListController', TodoListController);

})();
