(function() {
    'use strict';

    function TodoListService() {
      var TodoListService = {};

        TodoListService.add = function(todos, title) {
          todos.push({
            'title': title,
            'isComplite': false
          });
        };

      return TodoListService;
    }

angular
  .module('todoApp')
  .service('TodoListService', TodoListService);
})();
