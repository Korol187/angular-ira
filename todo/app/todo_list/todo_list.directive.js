(function() {
  'use strict';

  function todoList() {
    return {
      restrict: 'E',
      controller: 'TodoListController',
      controllerAs: 'vm',
      templateUrl: 'app/todo_list/todo_list.html',
      replace: true,
      bindToController: true,
      scope: {
        todos: '='
      }
    }
  }

  angular
    .module('todoApp')
    .directive('todoList', todoList);
})();
