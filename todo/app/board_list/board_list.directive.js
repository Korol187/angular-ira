(function() {
  'use strict';

  function boardList() {
    return {
      restrict: 'E',
      controller: 'BoardListController',
      controllerAs: 'vm',
      templateUrl: 'app/board_list/board_list.html',
      replace: true,
      bindToController: true
    }
  }

  angular
    .module('todoApp')
    .directive('boardList', boardList);

})();
