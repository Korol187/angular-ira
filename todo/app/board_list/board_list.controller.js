(function() {
  'use strict';

  BoardListController.$inject = ['BoardListService'];

  function BoardListController(BoardListService) {
    var vm = this;
    vm.boards = [];
    vm.newBoardTitle = '';
    vm.BoardListService = BoardListService;

    //get boards
    vm.BoardListService.getData().then(function(data) {
      vm.boards = data;
    });
  }

  //add new board
  BoardListController.prototype.add = function() {
    this.BoardListService.add(this.boards, this.newBoardTitle);
    this.newBoardTitle = '';
  }

  //remove board
  BoardListController.prototype.remove = function(board) {
    var index = this.boards.findIndex(function(item) {
      return item.id === board.id;
    })
    this.boards.splice(index, 1);
  };

  angular
    .module('todoApp')
    .controller('BoardListController', BoardListController);
})();
