(function() {
  'use strict';

  BoardListService.$inject = ['$http'];
  function BoardListService($http) {
    var BoardListService = {};

    BoardListService.getData = function() {
      var promise = $http.get('todo.json').then(function(response) {
        return response.data;
      });
      return promise;
    };

    BoardListService.add = function(boards, title) {
        var id = _generateId();
        boards.push({
          "id": id,
          "title": title,
          "todos": []
        });
      }
      //generate id
    function _generateId() {
      var i, random;
      var id = '';
      for (i = 0; i < 32; i++) {
        random = Math.random() * 16 | 0;
        if (i === 8 || i === 12 || i === 16 || i === 20) {
          id += '-';
        }
        id += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random)).toString(16);
      }
      return id;
    }

    return BoardListService;
  }

  angular
    .module('todoApp')
    .service('BoardListService', BoardListService);
})();
