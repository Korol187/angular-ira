import ng from 'angular';

import ContentComponent from './content.component';
import contentService from './content.service';

export default ng.module('app.content', [])
  .component('content', ContentComponent)
  .service('contentService', contentService)
  .name;
