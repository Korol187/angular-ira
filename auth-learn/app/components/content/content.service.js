export default class contentService {
  static $inject = ['$http'];
  constructor($http) {
    this.$http = $http;
  }

  getSecret () {
    return this.$http.get(API_URL + 'users/secret', { withCredentials: true });
  }
}
