import template from './content.html';
import contentService from './content.service';

class ContentController {
  static $inject = ['contentService'];
  constructor(contentService) {
    this.contentService = contentService;
  }

  $onInit() {
    this.getSecret();
  }

  getSecret () {
    this.contentService.getSecret().then(result => console.log(result.data));
  }
}

export default {
  template,
  controller: ContentController,
}
