import ng from 'angular';

import ngHello from './hello';
import ngContent from './content';
import ngAuth from './auth';

export default ng.module('app.components', [ngHello, ngContent, ngAuth])
  .name;
