import ng from 'angular';

import AuthComponent from './auth.component';
import authService from './auth.service';


export default ng.module('app.auth', [])
  .component('auth', AuthComponent)
  .service('authService', authService)
  .name;
