import authService from './auth.service';

export default class AuthController {
  static $inject = ['authService', '$location', '$window', '$cookies'];
  constructor(authService, $location, $window, $cookies) {
    this.authService = authService;
    this.$window = $window;
    this.$location = $location;
    this.$cookies = $cookies;
  }

  $onInit() {
    this.authForm = false;
  }

  login(data) {
    var self = this;
    this.authService.login(data).then(
      function(response) {
        self.authService.user = {
          info: response.data,
          isLogged: true
        }
        self.error = '';
        self.authForm = false;
        self.formUser = {};
        self.$location.path('/content');
      },
      function(response) {
        self.error = response.data;
    });
  }

  logout() {
    var self = this;
    self.authService.logout().then(function(result) {
      self.authService.user = {
        info: {},
        isLogged: false
      }
      self.$location.path('/hello');
    });
  }
}
