import template from './auth.html';
import authController from './auth.controller'

export default {
  template,
  controller: authController,
}
