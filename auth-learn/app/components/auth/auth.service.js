export default class serviceAuth {
  static $inject = ['$http'];
  constructor($http) {
    this.$http = $http;
    this.user = {
      info: {},
      isLogged: false
    }
  }

  login(data) {
    return this.$http.post(API_URL + 'users/login', data, { withCredentials: true });
  };

  logout() {
    return this.$http.get(API_URL + 'users/logout');
  };

  getUser(id) {
    return this.$http.get(API_URL + 'users/' + id);
  }
}
