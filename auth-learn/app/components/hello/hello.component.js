import template from './hello.html';
import HelloService from './hello.service';

class HelloController {
  static $inject = ['HelloService'];
  constructor(HelloService) {
    this.HelloService = HelloService;
  }

  $onInit() {
    this.authResponse = {
      status: 401,
      error: this.HelloService.error
    }
  }

}

export default {
  template,
  controller: HelloController,
}
