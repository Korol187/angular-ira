import ng from 'angular';

import HelloComponent from './hello.component';
import HelloService from './hello.service';

export default ng.module('app.hello', [])
  .component('hello', HelloComponent)
  .service('HelloService', HelloService)
  .name;
