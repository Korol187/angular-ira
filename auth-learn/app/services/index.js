import ng from 'angular';
import sessionService from './session.service';

export default ng.module('app.services', [])
  .service('sessionService', sessionService)
  .name;
