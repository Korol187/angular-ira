export const AppConfig = ($urlRouterProvider, $stateProvider) => {
  'ngInject';
  $stateProvider
    .state('hello', {
      url: '/hello',
      template: '<hello></hello>'
    })
    .state('content', {
      url: '/content',
      template: '<content></content>'
    });

    $urlRouterProvider.otherwise('/hello');
};
