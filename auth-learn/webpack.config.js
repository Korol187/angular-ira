const CleanPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');


module.exports = {
  context: __dirname,
  entry: {
    app: ['./index.js']
  },
  output: {
    path: './build',
    filename: 'bundle-[hash].js'
  },
  module: {
    loaders: [
      { test: /\.html$/, loader: "html" },
      { test: /\.js$/,loader: 'ng-annotate!babel?presets[]=es2015&presets[]=stage-0', exclude: /node_modules/ },
      { test: /\.less/, loader: ExtractTextPlugin.extract('style', 'css!less?sourceMap') },
      { test: /\.css$/, loader: ExtractTextPlugin.extract('style', 'css') },
      { test: /\.(ttf|eot|svg)(.*)?$/, loader: 'file?name=fonts/[name].[ext]'},
      { test: /\.(png|jpg|gif|woff|woff2)$/, loader: 'url?limit=10000&name=img/[name].[ext]' }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
     API_URL: JSON.stringify('http://auth.com:3000/')
    }),
    new CleanPlugin(['build']),
    new ExtractTextPlugin('bundle.css'),
    new HtmlWebpackPlugin({
      template: './index.html',
      inject: 'body'
    }),
  ],

};
