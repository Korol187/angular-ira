import 'bootstrap/less/bootstrap.less';
import './assets/css/style.less';
import ng from 'angular';
import uiRouter from 'angular-ui-router';
import ngCookies from 'angular-cookies';
import { AppConfig } from './index.config';
import Components from './app/components';
import Services from './app/services';
import HelloService from './app/components/hello/hello.service';

ng.module('app', [uiRouter, Components, Services, ngCookies])
  .config(AppConfig)
  .factory('authHttpResponseInterceptor', ['$q', '$location', 'HelloService',
    function($q, $location, HelloService) {
      return {
        response: function(response) {
          if (response.status === 401) {
            HelloService.error = 'You must be logged to access this page';
          }
          return response || $q.when(response);
        },
        responseError: function(rejection) {
          if (rejection.status === 401) {
            $location.path('/hello');
            HelloService.error = 'You must be logged to access this page';
          }
          return $q.reject(rejection);
        }
      }
    }
  ])
  .config(['$httpProvider', function($httpProvider) {
    //Http Intercpetor to check auth failures for xhr requests
    $httpProvider.interceptors.push('authHttpResponseInterceptor');
  }]);

export default 'app';
