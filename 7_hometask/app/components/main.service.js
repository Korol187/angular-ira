(function() {
    'use strict';

    function MainService ($firebaseArray) {
        var refCountry = new Firebase('https://tourlistantago.firebaseio.com/countries'),
            refTours = new Firebase('https://tourlistantago.firebaseio.com/tours'),
            refHotels = new Firebase('https://tourlistantago.firebaseio.com/hotels'),
            mainService = {};

        var country = $firebaseArray(refCountry);
        var hotels = $firebaseArray(refHotels);
        var tours = $firebaseArray(refTours);

        //получение данных

        function _getTours() {
            return tours;
        }
        function _getHotels() {
            return hotels;
        }
        function _getCountry() {
            return country;
        }
        function _oneCountry(tour) {

        }
        function _generateId() {
            var i, random;
            var uuid = '';
            for (i = 0; i < 32; i++) {
                random = Math.random() * 16 | 0;
                if (i === 8 || i === 12 || i === 16 || i === 20) {
                    uuid += '-';
                }
                uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random)).toString(16);
            }
            return uuid;
        }

        mainService.getTours = _getTours;
        mainService.getHotels = _getHotels;
        mainService.getCountry = _getCountry;
        mainService.oneCountry = _oneCountry;
        mainService.generateId = _generateId;

        return mainService;
    }

    MainService.$inject = ['$firebaseArray'];

    angular
        .module('myApp')
        .factory('MainService', MainService);

})();