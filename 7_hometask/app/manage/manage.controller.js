(function() {
    'use strict';

    function ManageCtrl (MainService, $scope, $timeout) {
        var self = $scope;
        self.newTour = {};
        self.newHotel = {};
        self.newCountry = {};
        self.sortType = '$index';
        self.sortReverse = false;
        self.flag = -1;
        self.flag_2 = -1;

        //список путевок
        self.tourList = MainService.getTours();

        //список стран
        self.countries = MainService.getCountry();

        //список отелей
        self.hotels = MainService.getHotels();

        //добавление нового тура
        self.addTour = function(tour) {
            self.tourList.$add({
                name: tour.name,
                photo: tour.photo,
                description: tour.description,
                countryId: tour.countryId,
                hotelId: tour.hotelId,
                duration: tour.duration,
                cost: tour.cost
            }).then(function() {
                self.newTour = {};
            })
        };

        //удаление тура
        self.deleteTour = function(tour) {
            self.tourList.$remove(tour);
        };

        //редактирование тура
        self.editTour = function(tour) {

        };


        //добавление страны
        self.addCountry = function(country) {
            self.countries.$add({
                id: MainService.generateId(),
                name: country.name,
                photo: country.photo
            }).then(function() {
                self.newCountry = {};
            })
        };

        //удаление страны
        self.deleteCountry = function (deletedCountry) {
            var indexDeleteCountry = deletedCountry.id;
            var countryIdList = []; //id стран которые используются в путевках
            angular.forEach(self.tourList, function (value) {
                var id = value.countryId;
                countryIdList.push(id);
            });
            if (countryIdList.indexOf(indexDeleteCountry) != -1) {
                self.flag_2 = 1;
                var timer = $timeout(function(){
                    self.flag_2 = -1;
                    $timeout.cancel(timer);
                }, 3000);
            } else {
                self.flag_2 = -1;
                self.countries.$remove(deletedCountry);
            }
        };

        //добaвление отеля
        self.addHotel = function(hotel) {
            self.hotels.$add({
                id: MainService.generateId(),
                name: hotel.name,
                photo: hotel.photo,
                category: hotel.category,
                description: hotel.description
            }).then(function() {
                self.newHotel = {};
            })
        };

        //удаление отеля
        self.deleteHotel = function (deletedHotel) {
            var indexDeleteHotel = deletedHotel.id;
            var hotelIdList = []; //id отелей которые используются в путевках
            angular.forEach(self.tourList, function (value) {
                var id = value.hotelId;
                hotelIdList.push(id);
            });
            if (hotelIdList.indexOf(indexDeleteHotel) != -1) {
                self.flag = 1;
                var timer = $timeout(function(){
                    self.flag = -1;
                    $timeout.cancel(timer);
                }, 3000);
            } else {
                self.flag = -1;
                self.hotels.$remove(deletedHotel);
            }
        };
    }
    ManageCtrl.$inject = ['MainService', '$scope', '$timeout'];

    angular
        .module('myApp')
        .controller('ManageCtrl', ManageCtrl);

})();