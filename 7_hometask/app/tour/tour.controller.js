(function () {
    'use strict';

    function TourCtrl(MainService, $routeParams) {
        var self = this;
        var id = $routeParams.tourId;

        //список путевок
        self.tourList = MainService.getTours();

        //одна путевка
        self.tourList.$loaded().then(function () {
            angular.forEach(self.tourList, function (value) {
                if (value.$id == id) {
                    self.tour = value;
                }
            });
            getCountry(self.tour);
            getHotel(self.tour);
        });

        //список стран
        function getCountry(tour) {
            self.countries = MainService.getCountry();
            self.countries.$loaded().then(function () {
                angular.forEach(self.countries, function (value) {
                    if (value.id == tour.countryId) {
                        self.country = value;
                    }
                });
                return self.country;
            });
        }

        //список отелей
        function getHotel(tour) {
            self.hotels = MainService.getHotels();
            self.hotels.$loaded().then(function () {
                angular.forEach(self.hotels, function (value) {
                    if (value.id == tour.hotelId) {
                        self.hotel = value;
                    }
                });
                return self.hotel;
            });
        }
    }

    TourCtrl.$inject = ['MainService', '$routeParams'];

    angular
        .module('myApp')
        .controller('TourCtrl', TourCtrl);

})();