(function() {
    'use strict';

    function tourlist () {
        return {
            restrict: 'E',
            controller: 'TourListCtrl',
            controllerAs: 'tourlistCtrl',
            templateUrl:'app/tourlist/tourlistTpl.html',
            replace: true
        }
    }

    angular
        .module('myApp')
        .directive('tourlist', tourlist);

})();
