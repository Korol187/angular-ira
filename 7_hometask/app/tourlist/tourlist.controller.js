(function() {
    'use strict';

    function TourListCtrl (MainService) {
        var self = this;
        self.selectedStar = '';
        self.selectedCountry = '';
        self.selectedCost = '';

        //список путевок
        self.tourList = MainService.getTours();

        //список стран
        self.countries = MainService.getCountry();

        //список отелей
        self.hotels = MainService.getHotels();

        self.resetAll = function() {
            self.selectedStar = '';
            self.selectedCountry = '';
            self.selectedCost = '';
        }
    }
    TourListCtrl.$inject = ['MainService'];

    angular
        .module('myApp')
        .controller('TourListCtrl', TourListCtrl);

})();