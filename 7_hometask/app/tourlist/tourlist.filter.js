(function () {
    'use strict';

    angular.module('myApp')
        .filter('CountryFilter', function () {
            return function (tourList, selectedCountry) {
                if (tourList != undefined && selectedCountry !== '') {
                    var newTourList = [];
                    for (var i = 0; i < tourList.length; i++) {
                        var item = tourList[i];
                        var id = item.countryId;
                        if (selectedCountry == id) {
                            newTourList.push(item);
                        }
                    }
                    return newTourList;
                }
                return tourList;
            }
        })
        .filter('CostFilter', function () {
            return function (tourList, selectedCost) {
                if (tourList != undefined && selectedCost !== '') {
                    if (selectedCost == 2) {
                        tourList.sort(function (a, b) {
                            if (a.cost > b.cost) {
                                return 1;
                            }
                            if (a.cost < b.cost) {
                                return -1;
                            }
                        });
                    }
                    if (selectedCost == 1) {
                        tourList.sort(function (a, b) {
                            if (a.cost < b.cost) {
                                return 1;
                            }
                            if (a.cost > b.cost) {
                                return -1;
                            }
                        });
                    }
                }
                return tourList;
            }
        })
        .filter('StarsFilter', function () {
            return function (tourList, selectedStar, hotels) {
                if (tourList != undefined && selectedStar !== '') {
                    var newTourList = [];
                    var tourHotels = []; //массив отелей которые необходимо будет отсортировать
                    for (var i = 0; i < hotels.length; i++) {
                        var hotelStar = hotels[i].category;
                        if (hotelStar == selectedStar) { //ищем отели с необходимой звездой
                            tourHotels.push(hotels[i].id);
                        }
                    }
                    for (var j = 0; j < tourList.length; j++) {
                        var tour = tourList[j];
                        var idHotel = tourList[j].hotelId;  //отель, который стоит в путевке
                        if (tourHotels.indexOf(idHotel) != -1) {  //ищем отель среди отобраных отелей
                            newTourList.push(tour);  //формируем новый список отелей
                        }
                    }
                    return newTourList;
                }
                return tourList;
            }
        });

})();