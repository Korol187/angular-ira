(function() {
    'use strict';

    function routerConfig($routeProvider) {
        var tourlist = {
                templateUrl: 'app/tourlist/tours-main.html',
                controller: 'TourListCtrl as tourlistCtrl'
            },
            tour = {
                templateUrl: 'app/tour/tour.html',
                controller: 'TourCtrl as tourCtrl'
            },
            manage = {
                templateUrl: 'app/manage/manage.html',
                controller: 'ManageCtrl'
            },
            defaults = {
                redirectTo: '/'
            };

        $routeProvider
            .when('/', tourlist)
            .when('/tour/:tourId', tour)
            .when('/manage', manage)
            .otherwise( defaults );
    }

    routerConfig.$inject = ['$routeProvider'];

    angular
        .module('myApp')
        .config(routerConfig);
})();
