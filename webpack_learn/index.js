import 'bootstrap/less/bootstrap.less';
import './assets/css/style.less';
import ng from 'angular';
import ngBoardList from './app/board-list';
import ngBoard from './app/board';
import ngTodoList from './app/todo-list';
import ngTodo from './app/todo';

ng.module('boardApp', [ngBoardList, ngTodoList, ngBoard, ngTodo]);

export default 'boardApp';
