import ng from 'angular';

import TodoComponent from './todo.component';

export default ng.module('app.todo', [])
  .component('todo', TodoComponent)
  .name;
