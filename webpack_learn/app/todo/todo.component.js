import template from './todo.html';

class TodoController {
  update() {
    this.TodoListController.update();
  }
}

export default {
  template,
  controller: TodoController,
  bindings: {
    todo: '='
  },
  require: {
    TodoListController: '^^todoList',
  }
}
