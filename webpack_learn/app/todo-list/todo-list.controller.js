export default class TodoListController {
  constructor() {}
  $onInit() {
    this.formData = {}
  }

  add(data) {
    this.BoardController.update(data);
    this.formData = {};
  }

  update(data) {
    this.BoardController.update(data);
    this.formData = {};
  }
}
