import template from './todo-list.html';
import TodoListController from './todo-list.controller';

export default {
  template,
  controller: TodoListController,
  bindings: {
    todos: '='
  },
  require: {
    BoardController: '^^board',
  }
}
