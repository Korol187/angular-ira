import ng from 'angular';
import TodoListComponent from './todo-list.component';

export default ng.module('app.todoList', [])
  .component('todoList', TodoListComponent)
  .name;
