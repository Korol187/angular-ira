export default class BoardListService {
  static $inject = ['$http'];
  constructor($http) {
    this.$http = $http;
  }

  get() {
    return this.$http.get(API_URL + 'boards')
  }

  add(data) {
    return this.$http.post(API_URL + 'boards/', data);
  }

  update(board, data) {
    return this.$http.put(API_URL + 'boards/' + board._id, data);
  }

  remove(board) {
    return this.$http.delete(API_URL + 'boards/' + board._id);
  }
}
