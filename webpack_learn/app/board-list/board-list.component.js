import template from './board-list.html';
import BoardListController from './board-list.controller';

export default {
    template,
    replace: true,
    controller: BoardListController
}
