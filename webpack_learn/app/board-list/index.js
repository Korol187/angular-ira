import ng from 'angular';

import BoardListComponent from './board-list.component';
import BoardListService from './board-list.service';

export default ng.module('app.boardList', [])
  .component('boardList', BoardListComponent)
  .service('BoardListService', BoardListService)
  .name;
