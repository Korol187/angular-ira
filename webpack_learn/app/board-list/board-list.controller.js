export default class BoardListController {
  static $inject = ['BoardListService'];
  constructor(BoardListService) {
    this.BoardListService = BoardListService;
  }

  $onInit() {
    this.formData = {};
    this.get();
  }

  get() {
    this.BoardListService.get().then(result => this.boards = result.data);
  }

  add() {
    this.BoardListService.add(this.formData)
      .then(result => this.get());
    this.formData = {};
  }

  update(board, data) {
    this.BoardListService.update(board, data)
      .then(result => this.get());
  }

  remove(board) {
    this.BoardListService.remove(board).then(result => this.get());
  }
}
