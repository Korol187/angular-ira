import template from './board.html';
import BoardController from './board.controller';

export default {
  template,
  controller: BoardController,
  bindings: {
    board: '='
  },
  require: {
    BoardListController: '^^boardList',
  }
}
