import ng from 'angular';

import BoardComponent from './board.component';

export default ng.module('app.board', [])
  .component('board', BoardComponent)
  .name;
