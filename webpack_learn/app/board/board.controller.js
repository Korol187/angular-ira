export default class BoardController {
  $onInit() {
    this.formData = {};
    this.isEdit = false;
  }

  remove() {
    this.BoardListController.remove(this.board);
  }

  update(data) {
    if (data && data.todo) {
      this.board.todos.push({
        title: data.todo
      })
    }
    this.formData = {
      title: data && data.title ? data.title : this.board.title,
      todos: this.board.todos
    }
    this.BoardListController.update(this.board, this.formData);
    this.formData = {};
    this.isEdit = false;
  }
}
