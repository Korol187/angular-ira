const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  title: String,
  todos: [{
    title: String,
    isComplite: { type: Boolean, default: false }
  }]
});

mongoose.model('Board', schema);
