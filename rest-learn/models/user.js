const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  name: String,
  email: String,
  password: String,
  information: String,
  role: { type: String, default: 'user' }
});

mongoose.model('User', schema);
