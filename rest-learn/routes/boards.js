const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Board = mongoose.model('Board');
const async = require('asyncawait/async');
const await = require('asyncawait/await');

router
  .get('/', async((req, res) => {
    const result = await (Board.find());
    res.json(result);
  }))
  .post('/', async((req, res) => {
    const board = new Board({
      title: req.body.title,
      todos: []
    });
    const result = await (board.save());
    res.json(result);
  }))
  .delete('/:id', async((req, res) => {
    const board = await (Board.findById(req.params.id));
    const result = await (board.remove());
    res.status(200).send('Board is deleted');
  }))
  .put('/:id', async((req, res) => {
    const board = await (Board.findById(req.params.id));
    board.title = req.body.title;
    board.todos = req.body.todos;
    const result = await (board.save());
    res.json(result);
  }))
  .get('/:id', async((req, res) => {
    const result = await (Board.findById(req.params.id));
    res.json(result);
  }))

module.exports = router;
