const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const User = mongoose.model('User');
const async = require('asyncawait/async');
const await = require('asyncawait/await');
const crypto = require('crypto');

router
  .get('/', async((req, res) => {
    const result = await (User.find());
    res.json(result);
  }))
  .post('/', async((req, res) => {
    const user = new User({
      name: req.body.name,
      email: req.body.email,
      password: crypto.createHash('md5').update(req.body.password).digest("hex"),
      information: req.body.information,
      role: req.body.role
    });
    const result = await (user.save());
    return res.json(result);
  }))
  .delete('/:id', async((req, res) => {
    const user = await (User.findById(req.params.id));
    const result = await (user.remove());
    return res.status(200).send('User is deleted');
  }))
  .post('/login', async((req, res) => {
    const password = crypto.createHash('md5').update(req.body.password).digest("hex");
    const user = await (User.findOne({ email: req.body.email }));
    if (!user || password !== user.password) {
      return res.status(400).send('Email or password wrong');
    }
    req.session.email = user.email;
    req.session.save();
    return res.json(user);
  }))
  .get('/secret', async((req, res) => {
    console.log(req.session.email)
    if (req.session.email) {
      return res.send('Session exist');
    }
    return res.status(401).send('No session');
  }))
  .get('/logout', async((req, res) => {
    req.session.destroy();
    return res.status(200).end();
  }));

module.exports = router;
