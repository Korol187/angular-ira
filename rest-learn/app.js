const express = require('express');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const log = require('./libs/log')(module);
const config = require('./config');
const cors = require('cors');
const session = require('express-session');
require('./libs/mongoose');
const corsOptions = {
  origin: 'http://auth.com:8080',
  credentials: true
};


const boards = require('./routes/boards');
const users = require('./routes/users');
const app = express();
app.set('trust proxy', 1);
const sessionOptions = {
  secret: 'Secret session',
  cookie: {
    maxAge: 36000000,
    httpOnly: true,
    secure: false
  }
};

// view engine setup
app.use(cors(corsOptions));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(session(sessionOptions));
app.use('/boards', boards);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.status(404);
  log.debug('Not found URL: %s', req.url);
  res.send({
    error: 'Not found'
  });
  return;
});

// error handlers
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  log.error('Internal error(%d): %s', res.statusCode, err.message);
  res.send({
    error: err.message
  });
  return;
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
